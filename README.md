# FMA 2014 Dokuwiki Theme

Dokuwiki theme for version Hrun 2014, and is online for future reference reasons.
This theme is not compatible with Dokuwiki version Greebo 2018 or later, refer for the FMA 2018 theme for that.

This theme is based on the default template for DokuWiki that was used until 2012.
It was extracted from the core late 2013 and since then not actively maintained by the core developers anymore.

[theme made by desbest](http://desbest.com)

![fma 2014 theme for dokuwiki screenshot](https://i.imgur.com/VtZPkba.png)

![example website using fma theme](https://i.imgur.com/JcIxd8n.png)